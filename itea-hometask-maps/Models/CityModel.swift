//
//  CityModel.swift
//  itea-hometask-maps
//
//  Created by Валентин Петруля on 6/17/19.
//  Copyright © 2019 Валентин Петруля. All rights reserved.
//

import Foundation
class City {
    var name: String
    var photo: String
    var latitude: Double
    var longitude: Double
    var zoom: Double
    var monuments: [Monument]
    
    init(name: String, photo: String, latitude: Double, longitude: Double, zoom: Double, monuments: [Monument]) {
        self.name = name
        self.photo = photo
        self.latitude = latitude
        self.longitude = longitude
        self.zoom = zoom
        self.monuments = monuments
    }
}
