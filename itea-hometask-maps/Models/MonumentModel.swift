//
//  MonumentModel.swift
//  itea-hometask-maps
//
//  Created by Валентин Петруля on 6/17/19.
//  Copyright © 2019 Валентин Петруля. All rights reserved.
//

import Foundation

class Monument {
    var name: String
    var latitude: Double
    var longitude: Double
    
    init(name: String, latitude: Double, longitude: Double) {
        self.name = name
        self.latitude = latitude
        self.longitude = longitude
    }
}
