//
//  CityCell.swift
//  itea-hometask-maps
//
//  Created by Валентин Петруля on 6/17/19.
//  Copyright © 2019 Валентин Петруля. All rights reserved.
//

import UIKit

class CityCell: UICollectionViewCell {
    
    @IBOutlet weak var cityImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    func update(city: City) {
        cityImageView.image = UIImage(named: city.photo)
        cityImageView.layer.cornerRadius = 10
        cityImageView.alpha = 0.81
        nameLabel.text = city.name
    }
}
