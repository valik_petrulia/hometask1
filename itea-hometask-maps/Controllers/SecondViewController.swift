//
//  SecondViewController.swift
//  itea-hometask-maps
//
//  Created by Валентин Петруля on 6/17/19.
//  Copyright © 2019 Валентин Петруля. All rights reserved.
//

import UIKit
import GoogleMaps

class SecondViewController: UIViewController {
    
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var monumentLabel: UILabel!
    @IBOutlet weak var mapView: GMSMapView!
    
    var city: City!
    var locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        roundCorners(view: mapView, corners: [.topLeft, .topRight], radius: 20)
        setCurrentLocation()
        setMarkers()
        cityLabel.text = city.name
        monumentLabel.text = ""
        // Do any additional setup after loading the view.
    }
    
}

extension SecondViewController: CLLocationManagerDelegate, GMSMapViewDelegate {
    func roundCorners( view: GMSMapView, corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: view.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        view.layer.mask = mask
    }
    
    func setCurrentLocation() {
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
        mapView.delegate = self
        mapView.isMyLocationEnabled = true
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let camera = GMSCameraPosition.camera(withLatitude: city.latitude, longitude: city.longitude, zoom: Float(city.zoom))
        self.locationManager.stopUpdatingLocation()
        
        self.mapView.animate(to: camera)
    }
    
    func setMarkers() {
        for monument in city.monuments {
            let marker = GMSMarker()
            let markerImage = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
            markerImage.image = UIImage(named: "marker")
            marker.iconView = markerImage
            marker.position = CLLocationCoordinate2D(latitude: monument.latitude, longitude: monument.longitude)
            marker.title = monument.name
            marker.map = mapView
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        monumentLabel.text = marker.title
        return true
    }
}
