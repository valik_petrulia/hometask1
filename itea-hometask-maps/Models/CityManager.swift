//
//  CityManager.swift
//  itea-hometask-maps
//
//  Created by Валентин Петруля on 6/23/19.
//  Copyright © 2019 Валентин Петруля. All rights reserved.
//

import Foundation

class CityManager {
    var array: [City] = []
    
    init() {
        let kyiv = City(name: "Киев", photo: "kyiv", latitude: 50.442154, longitude: 30.4714849, zoom: 12, monuments: [Monument(name: "Мариинский дворец", latitude: 50.4483985, longitude: 30.5354351), Monument(name: "Памятник Хмельницкому", latitude: 50.4535964, longitude: 30.5143384), Monument(name: "Родина-Мать", latitude: 50.4265826, longitude: 30.5608656), Monument(name: "Киево-Печерская лавра", latitude: 50.4353819, longitude: 30.5527279)])
        let warsaw = City(name: "Варшава", photo: "warsaw", latitude: 52.2336606, longitude: 20.9114961, zoom: 11.2, monuments: [Monument(name: "Королевский замок", latitude: 52.2479793, longitude: 21.0130673), Monument(name: "Музей Варшавского Восстания", latitude: 52.2323273, longitude: 20.9789653), Monument(name: "Музей Фредерика Шопена", latitude: 52.236543, longitude: 21.0207458), Monument(name: "Собор св. Яна", latitude: 52.2488475, longitude: 21.011433)])
        let rome = City(name: "Рим", photo: "rome", latitude: 41.8910337, longitude: 12.4271594, zoom: 11.7, monuments: [Monument(name: "Колизей", latitude: 41.8902142, longitude: 12.4900422), Monument(name: "Базилика Святого Петра", latitude: 41.9021707, longitude: 12.451748), Monument(name: "Римский форум", latitude: 41.8925302, longitude: 12.4830612), Monument(name: "Фонтан Треви", latitude: 41.9010333, longitude: 12.4810843)])
        let paris = City(name: "Париж", photo: "paris", latitude: 48.8519496, longitude: 2.2670616, zoom: 12, monuments: [Monument(name: "Эйфелева башня", latitude: 48.8583736, longitude: 2.2922926), Monument(name: "Лувр", latitude: 48.8608158, longitude: 2.3345625), Monument(name: "Триумфальная арка", latitude: 48.8737952, longitude: 2.2928388), Monument(name: "Базилика Сакре-Кёр", latitude: 48.8867081, longitude: 2.3409156)])
        let london = City(name: "Лондон", photo: "london", latitude: 51.4977803, longitude: -0.2506162, zoom: 11, monuments: [Monument(name: "Биг-Бен", latitude: 51.5007292, longitude: -0.1268141), Monument(name: "Лондонский Тауэр", latitude: 51.5081157, longitude: -0.078138), Monument(name: "Букингемский дворец", latitude: 51.514184, longitude: -0.1904866), Monument(name: "Британский музей", latitude: 51.5194166, longitude: -0.1291453)])
        let newYork = City(name: "Нью-Йорк", photo: "newyork", latitude: 40.6975854, longitude: -74.2598609, zoom: 10.5, monuments: [Monument(name: "Бруклинский мост", latitude: 40.7058175, longitude: -74.0003509), Monument(name: "Таймс сквер", latitude: 40.7579787, longitude: -73.9877313), Monument(name: "Метрополитен-музей", latitude: 40.7794411, longitude: -73.9807535), Monument(name: "Эмпайр-стейт-билдинг", latitude: 40.7485492, longitude: -73.9879522)])
        let moscow = City(name: "Москва", photo: "moscow", latitude: 55.7503233, longitude: 37.5513346, zoom: 13, monuments: [Monument(name: "Храм Василия Блаженного", latitude: 55.7525259, longitude: 37.6208981), Monument(name: "Большой театр", latitude: 55.7589536, longitude: 37.6178495), Monument(name: "Оружейная палата", latitude: 55.749769, longitude: 37.6112438), Monument(name: "ВДНХ", latitude: 55.8208319, longitude: 37.6370673)])
        
        array = [kyiv, warsaw, rome, paris, london, newYork, moscow]
    }
}
